# muzeme jednoduse importovat moduly ze standardni knihovny Pythonu - https://docs.python.org/3/library/
# staci zavolat poze 'import <nazev_vestaveneho_modulu>'
import math
import sys


class Kruh:
    PI = math.pi

    def __init__(self, polomer_v_cm):
        try:
            self.polomer = float(polomer_v_cm)
        except ValueError:
            sys.exit("Zadany polomer kruhu musi byt cislo!")

    def vypocitej_obvod(self):
        return 2 * self.PI * self.polomer

    def vypocitej_obsah(self):
        return self.PI * self.polomer ** 2


kruh = Kruh(polomer_v_cm=5)

print(kruh.vypocitej_obvod())
print(kruh.vypocitej_obsah())
