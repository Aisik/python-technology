# muzeme jednoduse importovat moduly ze standardni knihovny Pythonu - https://docs.python.org/3/library/
# importovany modul si muzeme nejak pojmenovat, treba 'm', muze se nam to hodit, protoze je to o neco kratsi
import math as m
# naopak 'sys' muzeme pojmenovat celym jmenen
import sys as system
# obecne ale neni dobre prejmenovavat moduly ze standardni knihovny, muzeme mast ostatni programatory


class Kruh:
    PI = m.pi

    def __init__(self, polomer_v_cm):
        try:
            self.polomer = float(polomer_v_cm)
        except ValueError:
            system.exit("Zadany polomer kruhu musi byt cislo!")

    def vypocitej_obvod(self):
        return 2 * self.PI * self.polomer

    def vypocitej_obsah(self):
        return self.PI * self.polomer ** 2


kruh = Kruh(polomer_v_cm=5)

print(kruh.vypocitej_obvod())
print(kruh.vypocitej_obsah())
