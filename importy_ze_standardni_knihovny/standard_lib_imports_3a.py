import time
import datetime

print(f"Pocet sekund od Unix Epoch Time (1.1.1970 00:00:00): {time.time()}")
print(f"Lepe citelne datum: {datetime.datetime.now()}")
print(f"Datum podle standardu ISO 8601: {datetime.datetime.now().isoformat()}")
