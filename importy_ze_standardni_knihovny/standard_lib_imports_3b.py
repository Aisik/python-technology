# krome klasickeho 'import <modul>', cimz importujeme cely modul, muzeme importovat pouze cast
# modulu (tridu, promennou, funkci, ...) pomoci 'from <modul> import <nazev>'

from time import time
from datetime import datetime

print(f"Pocet sekund od Unix Epoch Time (1.1.1970 00:00:00): {time()}")
print(f"Lepe citelne datum: {datetime.now()}")
print(f"Datum podle standardu ISO 8601: {datetime.now().isoformat()}")


# print("*** Cekam tri sekundy ***")
# aktualni_cas = datetime.now()
# sleep(3)
# cas_o_sekundu_dele = datetime.now()
# print("*** Hotovo ***")
# print(cas_o_sekundu_dele - aktualni_cas)


# vyhody: kod muze byt o neco citelnejsi, protoze volani je kratsi
# nevyhody: muze zmast ostatni (nevidime na prvni pohled, zda je funkce/trida importovana nebo ne)
#           nemuzeme pristupovat k celemu modulu a pokud chceme neco dalsiho, musime enumerovat (funkce time, sleep)

