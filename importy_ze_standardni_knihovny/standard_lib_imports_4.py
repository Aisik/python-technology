# krome klasickeho 'import <modul>', cimz importujeme cely modul, muzeme importovat vsechny objekty z modulu

from time import *
from datetime import *

# pocet sekund od Unix Epoch Time (1.1.1970 00:00:00)
print(time())
# lepe citelne datum
print(datetime.now())
# datum podle standardu ISO 8601
print(datetime.now().isoformat())


print("cekam tri sekundy")
aktualni_cas = datetime.now()
sleep(3)
cas_o_sekundu_dele = datetime.now()
print("hotovo")
print(cas_o_sekundu_dele - aktualni_cas)


# vyhody: kod muze byt o neco citelnejsi, protoze volani je kratsi
# nevyhody: muze zmast ostatni (nevidime na prvni pohled, zda je funkce/trida importovana nebo ne)
#           - zde plati dvojnasob, protoze importujeme vse a sance na kolizi s vlastnim jmenem je mnohem vetsi
#           nemuzeme pristupovat k celemu modulu a pokud chceme neco dalsiho, musime enumerovat (funkce time, sleep)

