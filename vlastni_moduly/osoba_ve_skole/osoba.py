class Osoba:
    def __init__(self, jmeno, prijmeni):
        self._jmeno = jmeno
        self._prijmeni = prijmeni

    def identifikuj_se(self):
        print(f"Jmenuji se {self._jmeno} {self._prijmeni}")
