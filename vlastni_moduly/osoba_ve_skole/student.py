# pouzivam relativni import, protoze chci importovat z tohoto package (baliku)
from .osoba import Osoba


class Student(Osoba):
    def __init__(self, jmeno, prijmeni):
        super().__init__(jmeno, prijmeni)
        self._seznam_znamek = []

    @property
    def seznam_znamek(self):
        return self._seznam_znamek

    @property
    def studijni_prumer(self):
        return sum(self._seznam_znamek) / len(self._seznam_znamek)

    def pridej_znamku(self, nova_znamka):
        self._seznam_znamek.append(nova_znamka)
