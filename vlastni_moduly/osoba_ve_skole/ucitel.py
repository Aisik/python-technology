# pouzivam relativni import, protoze chci importotovat z tohoto package (baliku)
from .osoba import Osoba
# pouzivam absolutni import z rootu (korene) projektu 'vlastni_moduly'
from nahoda import kostka, mince


class Ucitel(Osoba):
    def __init__(self, jmeno, prijmeni):
        super().__init__(jmeno, prijmeni)

    @staticmethod
    def _hodne_zkouseni(seznam_studentu):
        print("Dnes jsem se dobre vyspal, asi bude hodne jednicek. :)")
        for student in seznam_studentu:
            znamka = 1
            student.pridej_znamku(znamka)

    @staticmethod
    def _normalni_zkouseni(seznam_studentu):
        print("Dnes budu zkouset normalne.")
        for student in seznam_studentu:
            znamka = kostka.hod_kostkou()
            student.pridej_znamku(znamka)

    @classmethod
    def vyzkousej_studenty(cls, seznam_studentu):
        if mince.hod_minci():
            cls._hodne_zkouseni(seznam_studentu)
        else:
            cls._normalni_zkouseni(seznam_studentu)
