# modulu 'osoba_ve_skole' importuju tridy Student a Ucitel rovnou, protoze jsou importovany v __init__.py daneho modulu
# takze jsou takto primo pristupne
from osoba_ve_skole import Student, Ucitel


def nacti_studenty(cesta_k_seznamu_studentu):
    with open(cesta_k_seznamu_studentu) as soubor_studentu:
        seznam_jmen_studentu = [student.strip() for student in soubor_studentu.readlines()]

    seznam_studentu = []
    for student in seznam_jmen_studentu:
        jmeno, prijmeni = student.split(" ")
        seznam_studentu.append(Student(jmeno, prijmeni))
    return seznam_studentu


ucitel = Ucitel("Tomas", "Vondracek")
seznam_studentu = nacti_studenty("seznam_studentu.txt")

pocet_zkouseni = 0
while pocet_zkouseni < 5:
    ucitel.vyzkousej_studenty(seznam_studentu)
    pocet_zkouseni += 1


print("\n\nKonec roku a vysledne hodnoceni studentu:")
for student in seznam_studentu:
    print("*****")
    student.identifikuj_se()
    print(f"Moje znamky jsou: {student.seznam_znamek}")
    print(f"Muj prumer je: {student.studijni_prumer}")
    print("*****")

prumer_cele_tridy = sum([student.studijni_prumer for student in seznam_studentu]) / len(seznam_studentu)
print(f"Prumer cele tridy: {prumer_cele_tridy}")
